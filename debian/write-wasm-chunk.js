#!/usr/bin/node

const { readFileSync, writeFileSync } = require('node:fs');

const dest = process.argv.pop();
const src = process.argv.pop();

const base64 = readFileSync(src).toString('base64');
writeFileSync(dest, `'use strict'

const { Buffer } = require('node:buffer')

const wasmBase64 = '${base64}'

let wasmBuffer

Object.defineProperty(module, 'exports', {
  get: () => {
    return wasmBuffer
      ? wasmBuffer
      : (wasmBuffer = Buffer.from(wasmBase64, 'base64'))
  }
})`);
